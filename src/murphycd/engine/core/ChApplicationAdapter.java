package murphycd.engine.core;

import java.awt.Graphics;
import java.awt.event.WindowEvent;

import murphycd.engine.util.ChGraphics;

public class ChApplicationAdapter extends ChApplication {

	protected ChApplicationAdapter() {
		super();
	}

	private static final long serialVersionUID = 1L;

	/**
	 * ChApplication.init() is called immediately after ChApplication.start()
	 * and may be used to set ChApplication.Flag.visible to true (the field is
	 * false by default) or load resources. The main application thread is
	 * started after this method returns.
	 */
	@Override
	protected void init() {
	}

	/**
	 * Often called tick in other projects, this method is called a given number
	 * of times per second, called updates per second (ups), and handles the
	 * logic of the program.
	 */
	@Override
	protected void update() {
	}

	/**
	 * Draw graphics using the custom {@link ChGraphics} context g. Some
	 * standard methods in ChGraphics include clearing the screen and setting a
	 * background gradient. Note: Can make direct use of ChGraphics.pixels, type
	 * int[], which will be used to create the next BufferedImage after this
	 * method returns.
	 */
	@Override
	protected void render(ChGraphics g) {
	}

	/**
	 * Draw graphics using the standard {@link java.awt.Graphics} context g.
	 */
	@Override
	protected void draw(Graphics g) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		stop();
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

}
