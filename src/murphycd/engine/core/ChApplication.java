package murphycd.engine.core;

/**
 * The main body of the application, requires a driver class to start
 * @author Christopher Murphy
 */

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.lang.Thread.UncaughtExceptionHandler;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import murphycd.engine.util.ChGraphics;

public abstract class ChApplication extends Canvas implements Runnable, WindowListener, UncaughtExceptionHandler {

	private static final long serialVersionUID = 1L;

	public static class Attribute {
		public static String title = "ChEngine";
		public static int width = 320;
		public static int height = (int) (width / 16.0 * 9); // 180
		public static int updatesPerSecond = 30;
		public static double scale = 2; // default scale, 640x360

		public static int backgroundColor = 0;
		public static int bufferStrategy = 3;
		public static double averageFramePollingTime = 2;

		public static double getScaledWidth() {
			return width * scale;
		}

		public static double getScaledHeight() {
			return height * scale;
		}

		public static String getDisplayMode() {
			return Attribute.width + " x " + Attribute.height + " x " + Attribute.updatesPerSecond + ", scale: "
			        + Attribute.scale;
		}
	}

	public static class Flag {
		public static boolean verbose = false;
		public static boolean clearScreenOnRefresh = true;
		public static boolean visible = false;
		public static boolean resizable = false;
		public static boolean noborder = false;
	}

	private Thread thread;
	private JFrame frame;
	private ChGraphics chGraphics;
	private boolean running;
	private long nanoTime;

	private BufferedImage image;
	private int[] bufferPixels;

	// constructor
	protected ChApplication() {
		nanoTime = System.nanoTime();
		System.out.println("Initializing..." + Attribute.title);

		if (Flag.verbose) {
			System.out.println(Attribute.getDisplayMode());
		}
		Dimension size = new Dimension((int) (Attribute.width * Attribute.scale),
		        (int) (Attribute.height * Attribute.scale));
		setPreferredSize(size);
		frame = new JFrame();

		// create a drawable surface (Canvas uses Buffered image)
		image = new BufferedImage(Attribute.width, Attribute.height, BufferedImage.TYPE_INT_RGB);
		// convert the image object into a raster (drawable)
		bufferPixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

		chGraphics = new ChGraphics(Attribute.width, Attribute.height, Attribute.backgroundColor);
		chInitWindow();
		if (Flag.verbose) {
			System.out.println("Initialized! (" + (int) ((System.nanoTime() - nanoTime) / 1e3) / 1e3 + " ms)");
		}
	}

	public synchronized void start() {
		running = true;
		try {
			init();
		} catch (Throwable e) {
			uncaughtException(Thread.currentThread(), e);
		}
		thread = new Thread(this, Attribute.title);
		thread.setUncaughtExceptionHandler(this);
		thread.start();
	}

	public void uncaughtException(Thread t, Throwable e) {
		e.printStackTrace();
		JOptionPane.showMessageDialog(null,
		        "Unfortunately, " + Attribute.title + " has crashed unexpectedly. See the console for details.",
		        "Crash Report", JOptionPane.ERROR_MESSAGE);
		chStop(2);
	}

	public synchronized void stop() {
		running = false;
	}

	private void chStop(int status) {
		frame.dispose();
		try {
			thread.join(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Exiting with status code " + status);
		System.exit(status);
	}

	@Override
	public void run() { // called by thread.start()
		long lastTime = System.nanoTime(); // first system time
		long now; // current system time
		double delta = 0; // time passed
		long timer = System.currentTimeMillis(); // 1 second timer for UPS
		                                         // counter

		int frames = 0; // how many calls to render()
		int updates = 0; // count how many calls to update()
		double multiplier = Attribute.averageFramePollingTime; // number of
		                                                       // seconds to
		                                                       // collect data
		                                                       // on frames

		requestFocus();

		// print formated startup time: "Started! (000.000 ms)"
		System.out.println("Started! (" + (int) ((System.nanoTime() - nanoTime) / 1e3) / 1e3 + " ms)");

		if (Flag.visible)
			frame.setVisible(true);

		while (running) {
			now = System.nanoTime();
			delta += (now - lastTime) * Attribute.updatesPerSecond / 1e9;
			lastTime = now;
			while (delta >= 1) {
				chUpdate();
				updates++;
				delta--;
			}
			chRender();
			frames++;

			// display ups and fps
			if (Flag.verbose && System.currentTimeMillis() - timer >= 1000 * multiplier) {
				timer += 1000 * multiplier; // increment second
				System.out.println((int) (updates / multiplier) + " ups, " + (int) (frames / multiplier) + " fps");
				frame.setTitle(Attribute.title + " | " + (int) (updates / multiplier) + " ups, "
				        + (int) (frames / multiplier) + " fps");
				updates = 0;
				frames = 0;
			}
			Thread.yield();
		}
		chStop(0);
	}

	private void chUpdate() {
		update();
	}

	private void chRender() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(Attribute.bufferStrategy);
			return;
		}

		if (Flag.clearScreenOnRefresh)
			chGraphics.clearScreen();

		render(chGraphics);

		// get next frame
		for (int i = 0; i < chGraphics.pixels.length; i++) {
			bufferPixels[i] = chGraphics.pixels[i];
		}

		Graphics g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		draw(g);
		g.dispose();
		bs.show();
	}

	private void chInitWindow() {
		frame.setResizable(Flag.resizable);
		frame.setUndecorated(Flag.noborder);
		frame.setTitle(Attribute.title);
		frame.add(this);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setLocationRelativeTo(null); // centered
		frame.addWindowListener(this);
	}

	public long time() {
		return nanoTime;
	}

	protected void addToFrame(Component... components) {
		for (int i = 0; i < components.length; i++) {
			frame.add(components[i]);
		}
		frame.pack();
	}

	/**
	 * ChApplication.init() is called immediately after ChApplication.start()
	 * and may be used to set ChApplication.Flag.visible to true (the field is
	 * false by default) or load resources. The main application thread is
	 * started after this method returns.
	 */
	protected abstract void init();

	protected abstract void update();

	protected abstract void render(ChGraphics g);

	protected abstract void draw(Graphics g);

}
