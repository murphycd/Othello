package murphycd.engine.util;

public interface Drawable {

	void draw(java.awt.Graphics g);

}
