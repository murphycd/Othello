package murphycd.engine.util;

public interface ButtonClickHandler {

	public class ButtonClickEvent {

		public final String source;

		public ButtonClickEvent(String source) {
			this.source = source;
		}
	}

	public void buttonClicked(ButtonClickEvent event);

}
