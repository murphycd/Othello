package othello;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

import murphycd.engine.core.ChApplication.Attribute;
import murphycd.engine.util.Drawable;
import murphycd.engine.util.Input.MouseInputListener;

public class GameBoard implements Drawable, MouseInputListener {

	private static final int BLANK = 0;
	static final int WHITE = 1;
	static final int BLACK = 2;
	private boolean drawAllNextMoves = true; // TODO make button
	static int turnOfPlay;
	private static final int BOARD_SIZE = 8; // must be even, minimum 4

	private static final int TILE_SIZE_PADDING_FACTOR = 6;
	private static final int COLOR_FRAME_DARK = 0x007730;
	private static final int COLOR_FRAME_LIGHT = 0x00aa30;
	private static final int COLOR_TILE_HIGHLIGHT = 0x00dd30;
	private static final Color COLOR_WHITE = new Color(0xFCFCFC);
	private static final Color COLOR_BLACK = new Color(0x2D2D2D);
	private static final float GHOST_TILE_ALPHA = 0.6f;
	private static final Stroke STROKE_TILE_HIGHLIGHT = new BasicStroke(5f);
	private static final Stroke STROKE_DEFAULT = new BasicStroke(1f);

	private int[][] gridPieces;
	private Rectangle[][] gridLocations;
	private Rectangle frame;
	private double tileSize;
	private double tilePadding;
	private double tilePaddingHalf;
	private double boardAnchorX, boardAnchorY;
	private double pieceSize;

	private int selectedTileX = -1;
	private int selectedTileY = -1;

	private List<TileSequenceReport> listValidTurnMoves;


	public GameBoard() {

		double tileWidth = Attribute.getScaledWidth() / (BOARD_SIZE + 1);
		double tileHeight = Attribute.getScaledHeight() / (BOARD_SIZE + 1);
		tileSize = tileWidth < tileHeight ? tileWidth : tileHeight;

		tilePadding = tileSize / TILE_SIZE_PADDING_FACTOR;
		tilePaddingHalf = 0.5 * tilePadding;

		boardAnchorX = (Attribute.getScaledWidth() - (BOARD_SIZE * tileSize)) / 2.0;
		boardAnchorY = (Attribute.getScaledHeight() - (BOARD_SIZE * tileSize)) / 2.0;

		pieceSize = tileSize - tilePadding;

		initializeBoard();
	}

	private void initializeBoard() {
		// initialize board tiles
		gridLocations = new Rectangle[BOARD_SIZE][BOARD_SIZE];
		gridPieces = new int[BOARD_SIZE][BOARD_SIZE];
		for (int i = 0; i < gridPieces.length; i++) {
			for (int j = 0; j < gridPieces[0].length; j++) {
				gridPieces[i][j] = BLANK;
			}
		}

		// place starting tiles
		int middlePos1 = BOARD_SIZE / 2 - 1;
		int middlePos2 = BOARD_SIZE / 2;
		gridPieces[middlePos1][middlePos1] = WHITE;
		gridPieces[middlePos2][middlePos2] = WHITE;
		gridPieces[middlePos1][middlePos2] = BLACK;
		gridPieces[middlePos2][middlePos1] = BLACK;

		// evaluate next valid moves
		turnOfPlay = WHITE;
		listValidTurnMoves = calculateValidMovesOnGrid(turnOfPlay, gridPieces);

		// calculate frame location
		frame = new Rectangle((int) (boardAnchorX - tilePaddingHalf), (int) (boardAnchorY - tilePaddingHalf),
		        (int) (BOARD_SIZE * tileSize + 3 * tilePaddingHalf),
		        (int) (BOARD_SIZE * tileSize + 3 * tilePaddingHalf));

		// create grid tiles to house game pieces
		double x = 0.0;
		double y = 0.0;
		int s = (int) (tileSize - tilePaddingHalf);
		for (int i = 0; i < gridPieces.length; i++) {
			for (int j = 0; j < gridPieces[0].length; j++) {
				x = boardAnchorX + i * tileSize + tilePaddingHalf;
				y = boardAnchorY + j * tileSize + tilePaddingHalf;

				gridLocations[i][j] = new Rectangle((int) x, (int) y, s, s);
			}
		}
	}

	@Override
	public void draw(Graphics g) {

		/*
		 * Enable Anti-Aliasing
		 */
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/*
		 * Draw frame
		 */
		g2.setColor(new Color(COLOR_FRAME_DARK));
		g2.fillRoundRect(frame.x, frame.y, frame.width, frame.height, (int) tilePadding, (int) tilePadding);

		/*
		 * Draw tiles and game pieces
		 */
		Rectangle tile;
		for (int i = 0; i < gridPieces.length; i++) {
			for (int j = 0; j < gridPieces[0].length; j++) {
				tile = gridLocations[i][j];

				// draw tile
				g2.setColor(new Color(COLOR_FRAME_LIGHT));
				g2.fillRoundRect(tile.x, tile.y, tile.width, tile.height, (int) tilePadding, (int) tilePadding);

				// draw game piece
				if (gridPieces[i][j] != BLANK) {
					g2.setColor(gridPieces[i][j] == WHITE ? COLOR_WHITE : COLOR_BLACK);
					g2.fillOval((int) (tile.getCenterX() - (0.5 * pieceSize)),
					        (int) (tile.getCenterY() - (0.5 * pieceSize)), (int) pieceSize, (int) pieceSize);
				}

			}
		}
		
		/*
		 * Draw ghost piece on mouse hover
		 */
		for (TileSequenceReport report : listValidTurnMoves) {
			if (drawAllNextMoves || (report.selectedTileX == selectedTileX && report.selectedTileX == selectedTileY)) {
				tile = gridLocations[report.selectedTileX][report.selectedTileY];
				g2.setColor(turnOfPlay == WHITE ? COLOR_WHITE : COLOR_BLACK);
				drawGhostTile(g2, tile);
				g2.setColor(new Color(COLOR_FRAME_LIGHT));
				drawTileCapturePotential(g2, tile, report.capturedPieces.size());
			}
		}
		
		/*
		 * Highlight selected tile
		 */
		if (tileIndexIsOnBoard(selectedTileX, selectedTileY)) {
			tile = gridLocations[selectedTileX][selectedTileY];
			g2.setColor(new Color(COLOR_TILE_HIGHLIGHT));
			g2.setStroke(STROKE_TILE_HIGHLIGHT);
			g2.drawRoundRect(tile.x, tile.y, tile.width, tile.height, (int) tilePadding, (int) tilePadding);
			g2.setStroke(STROKE_DEFAULT);
		}

		drawDebugString(g2);
	}

	private void drawGhostTile(Graphics2D g2, Rectangle tile) {
		Composite composite = g2.getComposite();
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, GHOST_TILE_ALPHA));
		g2.fillOval((int) (tile.getCenterX() - (0.5 * pieceSize)), (int) (tile.getCenterY() - (0.5 * pieceSize)),
		        (int) pieceSize, (int) pieceSize);
		g2.setComposite(composite);
	}

	private void drawTileCapturePotential(Graphics2D g2, Rectangle tile, int capturePotential) {
		Font oldFont = g2.getFont();
		Font newFont = new Font(oldFont.getFontName(), Font.PLAIN, (int) (0.5 * tileSize));
		g2.setFont(newFont);

		String str = "" + capturePotential;
		int width = g2.getFontMetrics().stringWidth(str);
		int height = g2.getFontMetrics().getHeight();

		g2.drawString(str, (int) (tile.getCenterX() - 0.5 * width), (int) (tile.getCenterY() + 0.25 * height));

		g2.setFont(oldFont);
	}

	@SuppressWarnings("unused")
	private void drawDebugMarker(int x, int y, Graphics g, Color c, int offset) {
		g.setColor(c);
		g.drawLine(x - offset, y - offset, x + offset, y + offset);
		g.drawLine(x - offset, y + offset, x + offset, y - offset);
	}

	private void drawDebugString(Graphics g) {
		g.setColor(Color.BLACK);
		int textX = 16;
		int textY = 16;
		g.drawString("Width:  " + Attribute.width, textX, textY);
		textY += 16;
		g.drawString("Height: " + Attribute.height, textX, textY);
		textY += 16;
		g.drawString("Scaled Width:  " + Attribute.getScaledWidth(), textX, textY);
		textY += 16;
		g.drawString("Scaled Height: " + Attribute.getScaledHeight(), textX, textY);
		textY += 16;
		g.drawString("Anchor X: " + boardAnchorX, textX, textY);
		textY += 16;
		g.drawString("Anchor Y: " + boardAnchorY, textX, textY);
		textY += 16;
		g.drawString("tileSize: " + tileSize, textX, textY);
		textY += 16;
		g.drawString("tilePadding: " + tilePadding, textX, textY);
		textY += 16;
		g.drawString("tilePaddingHalf: " + tilePaddingHalf, textX, textY);
		textY += 16;
		g.drawString("pieceSize: " + pieceSize, textX, textY);
		textY += 16;
		g.drawString("selectedTileX: " + selectedTileX, textX, textY);
		textY += 16;
		g.drawString("selectedTileY: " + selectedTileY, textX, textY);
		textY += 16;
		g.drawString("numValidMoves: " + listValidTurnMoves.size(), textX, textY);
	}

	private void takeTurn(int tileIndexX, int tileIndexY) {
		// ensure move is valid
		TileSequenceReport move = null;
		for (TileSequenceReport report : listValidTurnMoves) {
			if (report.selectedTileX == tileIndexX && report.selectedTileY == tileIndexY) {
				move = report;
				break;
			}
		}

		if (move != null) {
			// place piece on grid
			gridPieces[tileIndexX][tileIndexY] = turnOfPlay;

			// flip captured pieces
			for (Point captured : move.capturedPieces) {
				gridPieces[captured.x][captured.y] = turnOfPlay;
			}

			advanceTurn();
		}
	}

	private void advanceTurn() {
		turnOfPlay = turnOfPlay == WHITE ? BLACK : WHITE;
		listValidTurnMoves = calculateValidMovesOnGrid(turnOfPlay, gridPieces);

		// if turnOfPlay color is a Computer Player, selected the best option and takeTurn
		// TODO implement Computer Player
		/*
		 * TreeMap<TileSequenceReport> tree; for (TileSequenceReport move :
		 * listValidTurnMoves) { move. }
		 */
	}

	private List<TileSequenceReport> calculateValidMovesOnGrid(int turnOfPlay, int[][] gridPieces) {
		List<TileSequenceReport> validGridMoves = new ArrayList<TileSequenceReport>();
		for (int i = 0; i < gridPieces.length; i++) {
			for (int j = 0; j < gridPieces[0].length; j++) {
				if (gridPieces[i][j] == BLANK) {
					TileSequenceReport seqReport = new TileSequenceReport(App.deepCopy(gridPieces), i, j);
					if (seqReport.capturedPieces.size() > 0) {
						validGridMoves.add(seqReport);
					}
				}
			}
		}
//		validGridMoves.sort(new Comparator<TileSequenceReport>() {
//			@Override
//			public int compare(TileSequenceReport first, TileSequenceReport second) {
//				return -1 * ((Integer) first.maxSeqVal).compareTo(second.maxSeqVal);
//			}
//		});
		return validGridMoves;
	}

	private boolean tileIndexIsOnBoard(int indexX, int indexY) {
		return indexX >= 0 && indexX < BOARD_SIZE && indexY >= 0 && indexY < BOARD_SIZE;
	}

	private boolean calculateSelectedTile(Point cursor) {

		boolean processed = frame.contains(cursor);

		if (processed) {
			boolean overlaps = false;
			Rectangle tile;
			for (int i = 0; i < BOARD_SIZE; i++) {
				for (int j = 0; j < BOARD_SIZE; j++) {

					tile = gridLocations[i][j];
					if (tile.contains(cursor)) {
						selectedTileX = i;
						selectedTileY = j;
						overlaps = true;
						break;
					} else {
						selectedTileX = -1;
						selectedTileY = -1;
					}

				}
				if (overlaps) {
					break;
				}
			}

		} else {
			selectedTileX = -1;
			selectedTileY = -1;
		}

		return processed;
	}

	@Override
	public boolean mouseClicked(MouseEvent event) {
		return calculateSelectedTile(event.getPoint());
	}

	@Override
	public boolean mouseEntered(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mousePressed(MouseEvent event) {
		return calculateSelectedTile(event.getPoint());
	}

	@Override
	public boolean mouseReleased(MouseEvent event) {
		boolean processed = calculateSelectedTile(event.getPoint());
		if (processed) {
			takeTurn(selectedTileX, selectedTileY);
		}
		return processed;
	}

	@Override
	public boolean mouseMoved(MouseEvent event) {
		return calculateSelectedTile(event.getPoint());
	}

	@Override
	public boolean mouseDragged(MouseEvent event) {
		return calculateSelectedTile(event.getPoint());
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent event) {
		return false;
	}

}
