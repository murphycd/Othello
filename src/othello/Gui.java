package othello;

import static murphycd.engine.core.ChApplication.Attribute.getScaledWidth;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import murphycd.engine.util.ButtonClickHandler;
import murphycd.engine.util.ButtonClickHandler.ButtonClickEvent;
import murphycd.engine.util.Drawable;
import murphycd.engine.util.Input.MouseInputListener;

/**
 * Visuals for User Interface
 */
public final class Gui implements Drawable, MouseInputListener {

	private final int TITLE_BAR_HEIGHT = 20;

	private ButtonClickHandler buttonClickHandler;

	@SuppressWarnings("unused")
	private TitleBar titleBar;
	private ExitButton exitButton;

	private class TitleBar implements Drawable {

		/**
		 * Title bar region is along the top of the screen, full width and
		 * height specified by Gui.
		 */
		private final Rectangle region;

		/**
		 * Flag whether the exit button should be illuminated (mouse hover).
		 */
		private boolean isFocused = false;

		private final Color unfocusedColor = new Color(0xFFFFFF);
		private final Color focusedColor = new Color(0x555555);
		
		private TitleBar(int height) {
			region = new Rectangle(0, 0, (int) (getScaledWidth() - height), height);
		}

		@Override
		public void draw(Graphics g) {
			final Color previous = g.getColor();

			if (isFocused) {
				g.setColor(Color.RED);
				g.fillRect(region.x, region.y, region.width, region.height);
			}

			g.setColor(isFocused ? focusedColor : unfocusedColor);
			g.drawLine(0, region.height, (int) getScaledWidth(), region.height);
			g.setColor(previous);
		}

		@SuppressWarnings("unused")
		private boolean overlaps(Point point) {
			return region.contains(point);
		}

	}

	private class ExitButton implements Drawable {

		/**
		 * Actual exit button image (cross) is drawn at the region width less
		 * any insets on all sides, where insets are calculated as the button
		 * width times the following factor.
		 */
		private final double borderPercentage = 0.2;

		/**
		 * Exit button region is a square in the upper-right of the screen whose
		 * sides are length
		 */
		private final Rectangle region;

		/**
		 * Flag whether the exit button should be illuminated (mouse hover).
		 */
		private boolean isFocused = false;

		private final Color unfocusedColor = new Color(0x555555);
		private final Color focusedColor = new Color(0xFFFFFF);

		/** Bounds for "X" mark */
		private int x1, y1, x2, y2;

		/**
		 * Create a new Exit Button (square) with the specified side length.
		 */
		private ExitButton(int size) {
			region = new Rectangle((int) (getScaledWidth() - size + 0.5), 0, size, size);

			calculateBounds(size);
		}

		private void calculateBounds(int size) {
			region.setBounds((int) (getScaledWidth() - size + 0.5) - 1, 1, size, size);

			// bounds for "X" mark
			final double x = region.getX() + (size * borderPercentage);
			final double y = region.getY() + (size * borderPercentage);
			final double w = size * (1 - 2 * borderPercentage);
			final double h = size * (1 - 2 * borderPercentage);
			x1 = (int) (x + 0.5);
			y1 = (int) (y + 0.5);
			x2 = (int) (x + w + 0.5);
			y2 = (int) (y + h + 0.5);
		}

		@Override
		public void draw(Graphics g) {
			final Color previous = g.getColor();

			if (isFocused) {
				g.setColor(Color.RED);
				g.fillRect(region.x, region.y, region.width, region.height);
			}

			g.setColor(isFocused ? focusedColor : unfocusedColor);
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1, y2, x2, y1);
			g.setColor(previous);
		}

		private void setFocused(boolean focused) {
			this.isFocused = focused;
		}

		private boolean overlaps(Point point) {
			return region.contains(point);
		}
	}

	Gui(ButtonClickHandler buttonClickHandler) {

		this.buttonClickHandler = buttonClickHandler;

		titleBar = new TitleBar(TITLE_BAR_HEIGHT);
		exitButton = new ExitButton(TITLE_BAR_HEIGHT);
	}

	private boolean isFocused() {
		return exitButton.isFocused;
	}

	private boolean onMouseButton(MouseEvent e) {
		boolean processed = false;
		// button click events
		if (e.getButton() == MouseEvent.BUTTON1) {
			if (exitButton.overlaps(e.getPoint())) {
				buttonClickHandler.buttonClicked(new ButtonClickEvent(AppData.BUTTON_EXIT));
				processed = true;
			}
		}
		return processed;
	}

	/** Drawable */
	@Override
	public void draw(Graphics g) {

		// titleBar.draw(g); // TODO not fully implemented
		exitButton.draw(g);
	}

	@Override
	public boolean mouseClicked(MouseEvent e) {
		return onMouseButton(e);
	}

	@Override
	public boolean mousePressed(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseReleased(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseEntered(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseMoved(MouseEvent e) {
		// button hover events
		exitButton.setFocused(exitButton.overlaps(e.getPoint()));
		return exitButton.isFocused;
	}

	@Override
	public boolean mouseDragged(MouseEvent e) {
		return isFocused();
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent e) {
		return isFocused();
	}
}
