package othello;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Records information about valid move sequences originating from a particular
 * tile given a particular board state.
 */
class TileSequenceReport {
	private int[][] boardState;
	int selectedTileX;
	int selectedTileY;
	List<Point> capturedPieces;

	TileSequenceReport(int[][] boardState, int selectedTileX, int selectedTileY) {
		this.boardState = boardState;
		this.selectedTileX = selectedTileX;
		this.selectedTileY = selectedTileY;
		capturedPieces = new ArrayList<Point>();

		calculate();
	}

	private void calculate() {
		// increasing x, same y
		evaluateSequence(1, 0);
		// increasing x, increasing y
		evaluateSequence(1, 1);
		// same x, increasing y
		evaluateSequence(0, 1);
		// decreasing x, increasing y
		evaluateSequence(-1, 1);
		// decreasing x, same y
		evaluateSequence(-1, 0);
		// decreasing x, decreasing y
		evaluateSequence(-1, -1);
		// same x, decreasing y
		evaluateSequence(0, -1);
		// increasing x, decreasing y
		evaluateSequence(1, -1);
	}

	/**
	 * Check that the line of sight in the given direction (defined by
	 * step-deltas) passes through an unbroken sequence of opponent's color
	 * without encountering BLANKS and finally terminating with the turnOfPlay
	 * color.
	 * 
	 * @param xStepDelta
	 * @param yStepDelta
	 */
	private void evaluateSequence(int xStepDelta, int yStepDelta) {

		int opponentColor = GameBoard.turnOfPlay == GameBoard.WHITE ? GameBoard.BLACK : GameBoard.WHITE;
		int checkIndexX = selectedTileX;
		int checkIndexY = selectedTileY;
		int count = 0;
		ArrayList<Point> candidateCapturedPieces = new ArrayList<Point>();

		checkIndexX += xStepDelta;
		checkIndexY += yStepDelta;
		// while tile being check remains within gameboard bounds
		while (checkIndexX >= 0 && checkIndexX < boardState.length && checkIndexY >= 0
		        && checkIndexY < boardState[0].length) {
			// beginning a sequence
			if (count == 0) {
				if (boardState[checkIndexX][checkIndexY] == opponentColor) {
					count++;
					candidateCapturedPieces.add(new Point(checkIndexX, checkIndexY));
				} else {
					break;
				}
			} else {
				// continuing a sequence
				if (boardState[checkIndexX][checkIndexY] == opponentColor) {
					count++;
					candidateCapturedPieces.add(new Point(checkIndexX, checkIndexY));
				} else if (boardState[checkIndexX][checkIndexY] == GameBoard.turnOfPlay) {
					// success
					capturedPieces.addAll(candidateCapturedPieces);
					break;
				} else {
					// invalidate this sequence
					break;
				}
			}
			checkIndexX += xStepDelta;
			checkIndexY += yStepDelta;
		}
	}
}
