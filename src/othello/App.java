package othello;

import java.awt.Color;
import java.awt.Graphics;

import murphycd.engine.core.ChApplicationAdapter;
import murphycd.engine.util.ButtonClickHandler;
import murphycd.engine.util.Input;

public class App extends ChApplicationAdapter implements ButtonClickHandler {
	private static final long serialVersionUID = 1L;

	private Input input;
	private Gui gui;
	private GameBoard gameBoard;

	protected void init() {

		input = new Input(this);
		gui = new Gui(this);
		gameBoard = new GameBoard();

		input.addMouseInputListener(gui);
		input.addMouseInputListener(gameBoard);

		Flag.visible = true;
	}

	protected void update() {
	}

	protected void draw(Graphics g) {

		gameBoard.draw(g);
		gui.draw(g);

		// window border
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, (int) Attribute.getScaledWidth() - 1, (int) Attribute.getScaledHeight() - 1);
	}

	@Override
	public void buttonClicked(ButtonClickEvent event) {
		switch (event.source) {
			case AppData.BUTTON_EXIT:
				stop();
				break;
			default:
				// do nothing
		}
	}

	/******************************************************************
	 * Description: Given an array, return a deep copy. The copy returned is
	 * independent of the input array: changing a value in the input array will
	 * not affect the copy once the copy is made. Changing a value in the copy
	 * will not affect the original either.
	 ****************************************************************/
	public static int[][] deepCopy(int[][] a) {
		int[][] b = new int[a.length][a[0].length];
		for (int x = 0; x < a.length; x++) {
			for (int y = 0; y < a[0].length; y++) {
				b[x][y] = a[x][y];
			}
		}
		return b;
	}
}
