package othello;

public class AppData {

	private static AppData instance;

	public static final String BUTTON_EXIT = "exit";

	public static AppData getInstance() {
		if (instance == null) {
			instance = new AppData();
		}
		if (!instance.isValid()) {
			System.err.println("Invalid App state.");
			System.exit(0);
		}
		return instance;
	}

	private AppData() {
	}

	public boolean isValid() {
		return true;
	}

}
